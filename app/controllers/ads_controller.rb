class AdsController < ApplicationController
	def index
		#@ads = YAML.load_file('config/ads.yml')
		@ads = Yamltohtml::Application.config.ads
	end

	def one
		@one_ad = Yamltohtml::Application.config.ads[params[:id].to_i]
	end
end
